/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */

package org.eclipse.osbp.utils.ui.classloader;

import org.eclipse.osbp.utils.classloader.IClassLoader;
import org.eclipse.osbp.utils.ui.internal.XtextServiceTrackerUtil;

/**
 * The Class ClassLoaderService.
 */
public class ClassLoaderService implements IClassLoader {

	/**
	 * Instantiates a new class loader service.
	 */
	public ClassLoaderService() {
		super();
	}

	/**
	 * OSBP-magic - clone the git repository:
	 * https://github.com/osbp/osbp-xtext-runtimebuilder.git - import the
	 * project into the Eclipse workspace:
	 * org.eclipse.osbp.xtext.builder.ui.access - un-comment the following
	 * source and import requestec classes - erlaubt das Laden einer Klasse, die
	 * per xtend generiert wurde
	 *
	 * @param className
	 *            the class name
	 * @return the class
	 */
	@Override
	public Class<?> loadClass(String className) {
		return loadClass(className, false);
	}

	/**
	 * OSBP-magic - clone the git repository:
	 * https://github.com/osbp/osbp-xtext-runtimebuilder.git - import the
	 * project into the Eclipse workspace:
	 * org.eclipse.osbp.xtext.builder.ui.access - un-comment the following
	 * source and import requestec classes - erlaubt das Laden einer Klasse, die
	 * per xtend generiert wurde
	 *
	 * @param className
	 *            the class name
	 * @return the class
	 */
	@Override
	public Class<?> reloadClass(String className) {
		return loadClass(className, true);
	}

	/**
	 * Load class.
	 *
	 * @param className
	 *            the class name
	 * @param reload
	 *            the reload
	 * @return the class
	 */
	private Class<?> loadClass(String className, boolean reload) {
		Class<?> clazz = null;
		XtextServiceTrackerUtil util = XtextServiceTrackerUtil.open();
		try {
			if	(util != null) {
				if	(reload) {
					clazz = util.getService().reloadClass(className);
				}
				else {
					clazz = util.getService().loadClass(className);
				}
			}				
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return clazz;
	}
}
