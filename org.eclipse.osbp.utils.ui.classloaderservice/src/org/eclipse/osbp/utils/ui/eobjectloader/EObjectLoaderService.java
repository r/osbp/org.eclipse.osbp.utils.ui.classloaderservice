/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */

package org.eclipse.osbp.utils.ui.eobjectloader;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.XtextResourceSet;

import org.eclipse.osbp.utils.classloader.IEObjectLoader;
import org.eclipse.osbp.utils.ui.internal.XtextServiceTrackerUtil;

/**
 * The Class EObjectLoaderService.
 */
public class EObjectLoaderService implements IEObjectLoader {

	/**
	 * Instantiates a new e object loader service.
	 */
	public EObjectLoaderService() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.utils.classloader.IEObjectLoader#getConfiguredXtextResourceFor(org.eclipse.emf.ecore.EClass, java.lang.String)
	 */
	@Override
	public XtextResourceSet getConfiguredXtextResourceFor(EClass eClass, String qualifiedName) {
		XtextResourceSet xtextResourceSet = null;
		XtextServiceTrackerUtil util = XtextServiceTrackerUtil.open();
		try {
			if	(util != null) {
				xtextResourceSet = util.getService().getConfiguredXtextResourceFor(eClass, qualifiedName);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return xtextResourceSet;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.osbp.utils.classloader.IEObjectLoader#getEObjectDescriptions(org.eclipse.emf.ecore.EClass, java.lang.String)
	 */
	@Override
	public Iterable<IEObjectDescription> getEObjectDescriptions(EClass eClass, String qualifiedName) {
		Iterable<IEObjectDescription> eObjectDescriptions = null;
		XtextServiceTrackerUtil util = XtextServiceTrackerUtil.open();
		try {
			if	(util != null) {
				eObjectDescriptions = util.getService().getEObjectDescriptions(eClass, qualifiedName);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return eObjectDescriptions;
	}

}
