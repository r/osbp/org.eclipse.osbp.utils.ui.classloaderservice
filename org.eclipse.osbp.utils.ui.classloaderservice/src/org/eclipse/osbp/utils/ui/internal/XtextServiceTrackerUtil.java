/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */

package org.eclipse.osbp.utils.ui.internal;

import javax.annotation.PreDestroy;

import org.eclipse.osbp.xtext.builder.ui.access.IXtextUtilService;
import org.osgi.util.tracker.ServiceTracker;

import org.eclipse.osbp.utils.ui.classloaderservice.Activator;

/**
 * The Class XtextServiceTrackerUtil.
 */
public class XtextServiceTrackerUtil {

	/** The tracker. */
	private final ServiceTracker<IXtextUtilService, IXtextUtilService> fTracker;
	
	/** The service. */
	private final IXtextUtilService fService;
	
	/**
	 * Instantiates a new xtext service tracker util.
	 *
	 * @param tracker
	 *            the tracker
	 * @param service
	 *            the service
	 */
	private XtextServiceTrackerUtil(ServiceTracker<IXtextUtilService, IXtextUtilService> tracker, IXtextUtilService service) {
		fTracker = tracker;
		fService = service;
	}
	
	/**
	 * Open the xtext service tracker util
	 *
	 * @return the xtext service tracker util
	 */
	public static XtextServiceTrackerUtil open() {
		XtextServiceTrackerUtil instance = null;

		ServiceTracker<IXtextUtilService, IXtextUtilService> tracker = new ServiceTracker<>(Activator.getContext(), IXtextUtilService.class, null);
		tracker.open();
		IXtextUtilService service = null;
		try {
			service = tracker.waitForService(5000);
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		if	(service != null) {
			instance = new XtextServiceTrackerUtil(tracker, service);
		}
		
		return instance;
	}
	
	/**
	 * Gets the service tracker.
	 *
	 * @return the service tracker
	 */
	public IXtextUtilService getService() {
		return fService;
	}
	
	/**
	 * Close the service tracker.
	 */
	@PreDestroy
	public void close() {
		try {
			if	(fTracker != null) {
				fTracker.close();
			}
		}
		catch (Exception e) { //NOSONAR - if close fails ignore it
			// NOP
		}
	}
}
